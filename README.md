# MotifJS

MotifJS is a JavaScript library.

* **Objects:** Build a full webpage with objects. 

## Examples
```
import MotifJS from '../../motifjs/dist/index.js';

const template =
{
	main: {
		header: {
			type: 'simple',
			sections: {
				views: [
					{
						title: [
							{ text: 'Hello World!' }
						],
					},
					{

						menu: [
							{ button: 'about.', type: 'simple' },
						],
						styles: {
							padding: '0px 10px',
							...MotifJS.Stylizer.constants.row,
						},
					},
				]
			},
		},
		content: {
			type: 'simple',
			styles: {
				backgroundColor: '#AAD8A8',
				...MotifJS.Stylizer.constants.fullContent,
			},
			sections: {
				scroll: 'vertical',
				views: [
					{
						details: [
							{ image: './colors/51-173-128.jpg' }, // dir in dist
						],
						styles: {
							...MotifJS.Stylizer.constants.screen.maxWidth,
						}
					},
				],
			},
		}

	}
};

MotifJS.render(template);
```

