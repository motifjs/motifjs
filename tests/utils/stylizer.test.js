import { Stylizer } from '../../src/utils';

describe('Stylizer', () => { 
	test('should detect camelCased string and transform into css readable properties', () => {
		expect(Stylizer.transformer.justifyContent).toBe('justify-content');
	});
	test('should set style string in css text', () => {
		const target = { style: { cssText: '' } };
		const css = 'padding: 10px';
		Stylizer.setTargetStyle(target, css);
		expect(target.style.cssText).toBe(css);
	});
});
