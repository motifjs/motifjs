import Templater from '../../src/utils/Templater';

const template = {
	main: {
		header: {
			type: 'sidebar',
			styles: { textColor: '#133468', width: '45vh' },
			section: {
				views: [
					{
						title: [
							{ text: 'test', styles: { fontSize: '2em' }}
						],
					},
					{
						menu: [
							{ text: 'about.' },
							{ text: 'main.' },
							{ text: 'contact.' },
						]
					}
				],
			},
		},
		content: {
			type: 'simple',
			styles: { backgroundColor: '#AAD8A8' },
			section: {
				scroll: 'vertical',
				views: [
					{
						details: [
							{
								text: 'hello.',
								styles: {
									backgroundColor: '#000',
								}
							},
						],
						styles: {
							height: '100vh',
						},
						scroll: 'horizontal',
					},
				],
			},
		},
	}
};

describe('Templater', () => { 
	test('should create a new instance of a Blank template if a template is not provided', () => {
		const templater = new Templater(template);
		const activeTemplate = templater.init(null, template.main);
		expect(activeTemplate.constructor.name).toBe('Blank');
	});
	test('should create a new instance of a Simple template', () => {
		const templater = new Templater(template);
		const activeTemplate = templater.init('simple', template.main);
		expect(activeTemplate.constructor.name).toBe('Simple');
	});
	test('should have 2 parent divs(parts): header and content', () => {
		const templater = new Templater(template);
		const { parts } = templater.init(null, template.main);
		expect(parts).toHaveLength(2);
	});
});
