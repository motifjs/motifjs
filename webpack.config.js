const path = require('path');

module.exports = {
	entry: './src/index.js',
	output: {
		filename: 'index.js',
		path: path.resolve(__dirname, 'dist'),
		libraryTarget: 'commonjs2'
	},
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
						"plugins": [
							[
								"@babel/plugin-proposal-class-properties",
								{ "loose": true },
							],
							["@babel/plugin-syntax-dynamic-import"]
						]
					}
				}
			}
		]
	}
};
