export default {
	scrollbar: {
		width: '20px',
	},
	scrollbarThumb: {
		background: '#fff',
		border: '2px solid black',
	},
};
