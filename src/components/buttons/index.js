import simple from './simple';

const mainStyles = `
	border: none;
	text-decoration: none;
	text-align: center;
	background: transparent;
	color: #000;
	font-family: sans-serif;
	outline: none;
	cursor: pointer;
	padding: 10px 20px;
`;

const styles = {
	simple
};

export default (style) =>`${mainStyles}${styles[style]}`;
