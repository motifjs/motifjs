const styles = {
	line: `
		display: block;
		width: 33px;
		height: 4px;
		margin-bottom: 5px;
		position: relative;
		background: #000;
		border-radius: 3px;
	`
};

export default `
	<span style='${styles.line}'></span>
	<span style='${styles.line}'></span>
	<span style='${styles.line}'></span>
 `;
