import burger from './burger';

const menus = {
	burger,
};

const styles = {
	half: {
		backgroundColor: '#000',
		position: 'absolute',
		top: '0',
		left: '0',
		minWidth: '50vw',
		minHeight: '100vh',
		zIndex: '8',
		opacity: '.8',
		display: 'flex',
		justifyContent: 'center',
		visibility: 'hidden',
	}
};

export default (type, style) => {
	return {
		menu: menus[type],
		parentStyles: styles[style],
	};
};
