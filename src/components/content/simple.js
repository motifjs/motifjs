export default {
	type: 'section',
	className: 'content',
	styles: {
		height: '100vh',
		width: '100vw',
		overflow: 'hidden auto',
	}
};
