export default {
	type: 'footer',
	className: 'footer',
	styles: {
		height: '10vh',
		width: '100vw',
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		color: 'black',
		position: 'fixed',
		bottom: 0,
		backgroundColor: '#fff',
	}
};
