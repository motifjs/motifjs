export default {
	type: 'footer',
	className: 'footer',
	styles: {
		height: '100vh',
		width: '5vw',
		display: 'flex',
		justifyContent: 'flex-start',
		flexCirection: 'column',
		alignItems: 'center',
		color: 'black',
		position: 'fixed',
		right: '0px',
		backgroundColor: '#fff',
	}
};
