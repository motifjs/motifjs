import simple from './simple';
import sidebar from './sidebar';

export default {
	simple,
	sidebar,
};
