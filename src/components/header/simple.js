export default {
	type: 'header',
	className: 'header',
	styles: {
		height: '10vh',
		width: '100vw',
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		color: 'black',
		position: 'fixed',
		top: 0,
		backgroundColor: '#fff',
	}
};
