export default {
	type: 'header',
	className: 'header',
	styles: {
		height: '100vh',
		width: '5vw',
		display: 'flex',
		justifyContent: 'space-between',
		flexDirection: 'column',
		color: 'black',
		position: 'fixed',
		left: 0,
		backgroundColor: '#fff',
	}
};
