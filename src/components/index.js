import header from './header';
import content from './content';
import footer from './footer';
import menus from './menus';
import buttons from './buttons';
import scrolls from './scrolls';

export {
	header,
	content,
	footer,
	menus,
	buttons,
	scrolls,
};
