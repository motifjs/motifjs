import { Templater, Stylizer } from './utils';

class MotifJS {
	constructor() {
		this.Stylizer = Stylizer;
	}

	render = (details) => {
		const templater = new Templater(details, this.Stylizer);
		const pages = templater.build();
		return pages;
	}
}

export default new MotifJS();
