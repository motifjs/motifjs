import simple from './simple';
import blank from './blank';

const templates = {
	simple,
	blank,
};

export default (type) => (parts) => new templates[type](parts);
