import { header, content, footer } from '../../components';

const full = { header, content, footer };

export default class Blank {
	constructor(details) {
		this.parts = Object.entries(details).reduce((acc, part) => {
			const [className, meta] = part;
			if (!meta.type) throw new Error(`${meta.type} is not a valid ${className} type`);
			const component = full[className][meta.type];
			acc.push(component);
			return acc;
		}, []);
	}
}
