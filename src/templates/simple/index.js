const full = {
	header: import('../../components/header/simple'),
	content: import('../../components/content/simple'),
	footer: import('../../components/footer/simple'),
};

export default class Simple {
	constructor(mainParts) {
		this.parts = Object.keys(mainParts).reduce((acc, part) => {
			if (full[part]) {
				const connect = full[part];
				acc.push(connect);
			}
			return acc;
		}, []);
	}
}
