import Builder from './Builder';
import templates from '../templates';

export default class Templater extends Builder {
	constructor(details, stylizer) {
		super();
		if (!details) return 'Must provide details.';
		this.Stylizer = stylizer;
		this.details = details;
		this.activeTemplate = null;
	}

	/**
	 * @param {string} template
	 * @param {object} main
	 */
	init = (template, main) => {
		this.activeTemplate = (!template)
			? templates('blank')(main)
			: templates(template)(main);
		return this.activeTemplate;
	}

	// todo size 
	renderImages = (renderTo, image, size) => {
		const div = this.createElement(renderTo, 'div', {});
		if (size) {
			const sizeToSet = this.sizeToSet(size);
			div.style.cssText += sizeToSet;
		}
		const img = new Image();
		img.src = image;
		img.style.cssText += 'max-height: 100%; min-width: 100%;';

		div.appendChild(img);
		return div;
	}

	renderTextContainer = (renderTo, singleOrArrayOfText) => {
		const target = !Array.isArray(singleOrArrayOfText) ? [singleOrArrayOfText] : singleOrArrayOfText;
		return target.map((txt) => {
			const { text } = txt;
			const div = this.createElement(renderTo, 'div', { innerHTML: text || txt });
			return div;
		});
	}

	renderSections = (renderTo, sections) => {
		const { scroll, views } = sections;
		if (scroll) this.Stylizer.setScroll(renderTo, scroll);
		if (!views) throw new Error('Must provide views in sections.');
		this.renderViews(renderTo, views);
	}

	renderButtomContainer = (renderTo, buttonContainer) => {
		const { type, button: text } = buttonContainer;
		const div = this.createElement(renderTo, 'button', { innerHTML: text });
		this.Stylizer.setButton(div, type);
		return div;
	}

	/**
	 * @param {object} container
	 * @param {object} view
	 * Parse child element
	 */
	renderView = (container, view) => {
		view.forEach((c) => {
			let result = null;
			const {
				text, styles: innerStyles, image, link, button,
			} = c;
			if (image) result = this.renderImages(container, image);
			if (text) result = this.renderTextContainer(container, text);
			if (button) result = this.renderButtomContainer(container, c);

			if (link) this.createLinkOnElement(result, link);
			if (innerStyles) {
				const target = !Array.isArray(result) ? [result] : result;
				target.forEach(t => this.Stylizer.stylize(t, innerStyles));
			}
		});
	}

	/**
	 * @param {object} renderTo
	 * @param {array} views
	 * Iterate array of views to insert into parent
	 */
	renderViews = (renderTo, views) => {
		if (!views.length) return;
		const [first] = views.splice(0, 1);
		const { scroll, styles, component, ...view } = first;
		const container = this.createElement(renderTo, 'div', { className });

		if (component) {
			const { details } = component;
			const { parentStyles } = this.Stylizer.setMenu(container, component);
			const nav = this.createElement(document.body, 'div', { className: 'nav' });
			this.Stylizer.stylize(nav, parentStyles);
			this.createVisibilityToggle(container, nav);
			return this.renderView(nav, details);
		}
		const [[className, targetView]] = Object.entries(view);

		if (scroll) this.Stylizer.setScroll(container, scroll);
		if (styles) this.Stylizer.stylize(container, styles);
		this.renderView(container, targetView);
		return this.renderViews(renderTo, views);
	}

	/**
	 * @param {object} targetDOM
	 * @param {object} toAdd
	 */
	// TO DO : TIME IS NOT EFFICENT 
	createChildrenDivs = (targetDOM, toAdd) => {
		Object.entries(toAdd).forEach((add) => {
			const [className, targetToAdd] = add;
			for (let i = 0; i < targetDOM.length; i++) {
				const curr = targetDOM[i];
				if (curr.className === className) {
					const { styles, sections } = targetToAdd;
					if (styles) this.Stylizer.stylize(curr, styles);
					this.renderSections(curr, sections);
					break;
				}
			}
		});
	}

	createParentDivs = (parts) => {
		parts.forEach((part, index) => {
			const { type, styles, ...rest } = part;
			const div = this.createElement(document.body, type, rest);
			if (styles) this.Stylizer.stylize(div, styles);
			this.activeTemplate.parts[index] = div;
		});
		return parts;
	}

	build = () => {
		const { template, main } = this.details;
		this.init(template, main);
		const { parts } = this.activeTemplate;
		this.createParentDivs(parts);
		this.createChildrenDivs(parts, main);

		const styled = this.Stylizer.stylizeParent(parts);
		const results = this.returnHTMLString(styled);

		return results.join('');
	}

}
