import Stylizer from './Stylizer';
import constants from './styleConstants';
import transformer from './styleTransformer';
import scrolls from './styleScrolls';
import { buttons, menus }from '../../components';

const dependencies = {
	constants,
	transformer,
	scrolls,
	buttons,
	menus,
};

export default new Stylizer(dependencies);
