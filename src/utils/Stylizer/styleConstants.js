const constants = {
	screen: { minHeight: '100vh', minWidth: '100vw' },
	fullContent: { minHeight: '100%', minWidth: '100vw' },
	halfSpread: { 
		display: 'flex',
		justifyContent: 'space-between',
		width: '50vw',
	},
	center: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
	},
	row: {
		display: 'flex',
		flexDirection: 'row',
	},
	column: {
		display: 'flex',
		flexDirection: 'column',
	}
};

export default constants;
