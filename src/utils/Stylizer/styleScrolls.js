import { scrolls as types } from '../../components';

const directions = {
	vertical: `
		overflow-y: auto;
  	`,
	horizontal:`
		display: flex;
  		overflow-x: auto;
  		overflow-y: hidden;
  	`
};


export default { types, directions };
