const pseudoElements = {
	scrollbar: '::-webkit-scrollbar',
	scrollbarTrack: '::-webkit-scrollbar-track',
	scrollbarThumb: '::-webkit-scrollbar-thumb',
	scrollarThumbHover: '::-webkit-scrollbar-thumb:hover',
};

const transformer = {
	backgroundColor: 'background-color',
	textColor: 'color',
	fontSize: 'font-size',
	justifyContent: 'justify-content',
	alignItems: 'align-items',
	borderBottom: 'border-bottom',
	paddingLeft: 'padding-left',
	paddingRight: 'padding-right',
	marginLeft: 'margin-left',
	marginRight: 'margin-right',
	maxWidth: 'max-width',
	minWidth: 'min-width',
	maxHeight: 'max-height',
	minHeight: 'min-height',
	flexDirection: 'flex-direction',
	zIndex: 'z-index',
	pseudoElements,
};

export default transformer;
