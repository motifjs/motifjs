export default class Stylizer {
	constructor(...dependencies) {
		Object.assign(this, ...dependencies);
	}

	stylizeParent = (parts) => {
		const parentStyles = this.extractParentsStyles(parts);
		this.setParentSizes(parentStyles, parts[1]);
		this.setParentPositions(parentStyles, parts[1]);
		this.applyReset();
		return parts;
	}

	/**
	 * Apply css reset to body
	 */
	applyReset = () => {
		document.body.style.cssText = `
			margin: 0; 
	   		padding: 0;
	   		border: 0;
	   		font-size: 100%;
	   		font: inherit;
	   		vertical-align: baseline;
	   		cursor: pointer;
		`;
	}

	extractParentsStyles = (parts) => {
		return parts.reduce((acc, curr) => {
			const { className, style } = curr;
			acc[className] = {
				width: style.width,
				height: style.height,
				top: style.top,
				bottom: style.bottom,
				right: style.right,
				left: style.left,
			};
			return acc;
		}, {});
	}

	setTargetStyle = (target, string) => {
		target.style.cssText += string;
	}

	setParentSizes = (parentStyles, middle) => {
		const { header, footer } = parentStyles;
		const { height, width } = [header, footer].filter(exist => exist).reduce((acc, curr) => {
			Object.entries(curr).forEach((c) => {
				const [type, num] = c;
				if (num && num.slice(0, 3) !== '100') {
					if (!acc[type]) acc[type] = num;
					else acc[type] += ` + ${num}`;
				}
			});
			return acc;
		}, {});

		const widthToCalc = `calc(${middle.style.width} - ${width})`;
		this.setTargetStyle(middle, `width: ${widthToCalc}`);

		const heightToCalc = `calc(${middle.style.height} - (${height}))`;
		this.setTargetStyle(middle, `height: ${heightToCalc}`);

		return middle;
	}

	setParentPositions = (parentStyles, middle) => {
		const { header, footer } = parentStyles;
		if (header) {
			const { top, height, left } = header;
			if (!top) this.setTargetStyle(middle, 'top: 0; position: fixed');
			else this.setTargetStyle(middle, `top: ${height}; position: fixed`);
			if (left) this.setTargetStyle(middle, `left: ${header.width}; position: fixed`);
		}
		return middle;
	}

	setScroll = (target, scroll) => {
		const { direction, type } = scroll;
		target.style.cssText += this.scrolls.directions[direction || 'vertical'];
		if (type) {
			const styles = this.scrolls.types[type];
			// this.stylizePsuedoElements(target, styles);
		}
	}

	/**
	 * @param {object} dom object
	 * @param {string} type of button
	 */
	setButton = (target, buttonType) => {
		const buttonStyles = this.buttons(buttonType);
		return target.style.cssText += buttonStyles;
	}

	setMenu = (parent, component) => {
		const { type, style } = component;
		const { menu, parentStyles } = this.menus(type, style);
		parent.innerHTML = menu;
		return { parentStyles };
	}

	stylizePsuedoElements = (target, psuedoStyles) => {
		return Object.entries(psuedoStyles).forEach((style) => {
			const [p, s] = style;
			const el = this.transformer.pseudoElements[p];
			const stringStyle = this.transformStyles(s);
			target.style.insertRule(`.${target.className}${el}{${stringStyle}}`, 0);
		});
	}

	/**
	 * @param {object} styles object of styles
	 * Takes an object of styles ({ fontSize: '1.5em' })
	 * @returns {string} 'font-size: 1.5em'
	 */
	transformStyles = (styles) => {
		return Object.entries(styles).reduce((acc, curr) => {
			if (!curr) return acc;
			let [attr, val] = curr;
			if (this.transformer[attr]) attr = this.transformer[attr];
			acc += (attr + ':' + val + ';');
			return acc;
		}, '');
	}

	/**
	 * @param {object} dom object
	 * @param {object} object of styles
	 */
	stylize = (target, styles) => {
		return target.style.cssText += this.transformStyles(styles);
	}

}
