export default class Builder {

	sizeToSet = (size) => {
		return `
			height: ${(!isNaN(Number(size))) ? size : 100}%;
			min-width: 100%;
		`;
	}

	/**
	 * @param {object} parent (dom element)
	 * @param {object} type (type of element)
	 * @param {object} additionalOptions (any additional element properties to add)
	 * Creates an element and appends to parent
	 */
	createElement = (parent, type, addtionalOptions = {}) => {
		let div = document.createElement(type);
		Object.assign(div, { ...addtionalOptions });
		parent.appendChild(div);
		return div;
	}

	returnHTMLString = (parts) => {
		return parts.map((part) => part.outerHTML);
	}

	/**
	 * @param {object} targetDiv
	 * @param {string} link
	 * Creates an 'on click' listener to target div to open new link
	 */
	createLinkOnElement = (targetDiv, link) => {
		targetDiv.onclick = () => window.open(link);
	}

	createVisibilityToggle = (targetDiv, divToToggle) => {
		targetDiv.onclick = () => {
			const [targetClass] = document.getElementsByClassName('nav');
			const currentVisibility = targetClass.style.visibility;
			targetClass.style.visibility = (currentVisibility === 'hidden') ? 'visible' : 'hidden';
			const [contentClass] = document.getElementsByClassName('content');
			contentClass.onclick = () => targetClass.style.visibility = 'hidden';
		};
	}
}
