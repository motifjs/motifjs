module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/components/buttons/index.js":
/*!*****************************************!*\
  !*** ./src/components/buttons/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _simple__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./simple */ \"./src/components/buttons/simple.js\");\n\nvar mainStyles = \"\\n\\tborder: none;\\n\\ttext-decoration: none;\\n\\ttext-align: center;\\n\\tbackground: transparent;\\n\\tcolor: #000;\\n\\tfont-family: sans-serif;\\n\\toutline: none;\\n\\tcursor: pointer;\\n\\tpadding: 10px 20px;\\n\";\nvar styles = {\n  simple: _simple__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (function (style) {\n  return \"\".concat(mainStyles).concat(styles[style]);\n});\n\n//# sourceURL=webpack:///./src/components/buttons/index.js?");

/***/ }),

/***/ "./src/components/buttons/simple.js":
/*!******************************************!*\
  !*** ./src/components/buttons/simple.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (\"\\n\\tborder-radius: 5px;\\n\\tfont-size: .75em;\\n\\tcolor: #000,\\n\");\n\n//# sourceURL=webpack:///./src/components/buttons/simple.js?");

/***/ }),

/***/ "./src/components/content/index.js":
/*!*****************************************!*\
  !*** ./src/components/content/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _simple__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./simple */ \"./src/components/content/simple.js\");\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  simple: _simple__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n});\n\n//# sourceURL=webpack:///./src/components/content/index.js?");

/***/ }),

/***/ "./src/components/content/simple.js":
/*!******************************************!*\
  !*** ./src/components/content/simple.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  type: 'section',\n  className: 'content',\n  styles: {\n    height: '100vh',\n    width: '100vw',\n    overflow: 'hidden auto'\n  }\n});\n\n//# sourceURL=webpack:///./src/components/content/simple.js?");

/***/ }),

/***/ "./src/components/footer/index.js":
/*!****************************************!*\
  !*** ./src/components/footer/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _simple__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./simple */ \"./src/components/footer/simple.js\");\n/* harmony import */ var _sidebar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sidebar */ \"./src/components/footer/sidebar.js\");\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  simple: _simple__WEBPACK_IMPORTED_MODULE_0__[\"default\"],\n  sidebar: _sidebar__WEBPACK_IMPORTED_MODULE_1__[\"default\"]\n});\n\n//# sourceURL=webpack:///./src/components/footer/index.js?");

/***/ }),

/***/ "./src/components/footer/sidebar.js":
/*!******************************************!*\
  !*** ./src/components/footer/sidebar.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  type: 'footer',\n  className: 'footer',\n  styles: {\n    height: '100vh',\n    width: '5vw',\n    display: 'flex',\n    justifyContent: 'flex-start',\n    flexCirection: 'column',\n    alignItems: 'center',\n    color: 'black',\n    position: 'fixed',\n    right: '0px',\n    backgroundColor: '#fff'\n  }\n});\n\n//# sourceURL=webpack:///./src/components/footer/sidebar.js?");

/***/ }),

/***/ "./src/components/footer/simple.js":
/*!*****************************************!*\
  !*** ./src/components/footer/simple.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  type: 'footer',\n  className: 'footer',\n  styles: {\n    height: '10vh',\n    width: '100vw',\n    display: 'flex',\n    justifyContent: 'space-between',\n    alignItems: 'center',\n    color: 'black',\n    position: 'fixed',\n    bottom: 0,\n    backgroundColor: '#fff'\n  }\n});\n\n//# sourceURL=webpack:///./src/components/footer/simple.js?");

/***/ }),

/***/ "./src/components/header/index.js":
/*!****************************************!*\
  !*** ./src/components/header/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _simple__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./simple */ \"./src/components/header/simple.js\");\n/* harmony import */ var _sidebar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sidebar */ \"./src/components/header/sidebar.js\");\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  simple: _simple__WEBPACK_IMPORTED_MODULE_0__[\"default\"],\n  sidebar: _sidebar__WEBPACK_IMPORTED_MODULE_1__[\"default\"]\n});\n\n//# sourceURL=webpack:///./src/components/header/index.js?");

/***/ }),

/***/ "./src/components/header/sidebar.js":
/*!******************************************!*\
  !*** ./src/components/header/sidebar.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  type: 'header',\n  className: 'header',\n  styles: {\n    height: '100vh',\n    width: '5vw',\n    display: 'flex',\n    justifyContent: 'space-between',\n    flexDirection: 'column',\n    color: 'black',\n    position: 'fixed',\n    left: 0,\n    backgroundColor: '#fff'\n  }\n});\n\n//# sourceURL=webpack:///./src/components/header/sidebar.js?");

/***/ }),

/***/ "./src/components/header/simple.js":
/*!*****************************************!*\
  !*** ./src/components/header/simple.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  type: 'header',\n  className: 'header',\n  styles: {\n    height: '10vh',\n    width: '100vw',\n    display: 'flex',\n    justifyContent: 'space-between',\n    alignItems: 'center',\n    color: 'black',\n    position: 'fixed',\n    top: 0,\n    backgroundColor: '#fff'\n  }\n});\n\n//# sourceURL=webpack:///./src/components/header/simple.js?");

/***/ }),

/***/ "./src/components/index.js":
/*!*********************************!*\
  !*** ./src/components/index.js ***!
  \*********************************/
/*! exports provided: header, content, footer, menus, buttons, scrolls */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header */ \"./src/components/header/index.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"header\", function() { return _header__WEBPACK_IMPORTED_MODULE_0__[\"default\"]; });\n\n/* harmony import */ var _content__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./content */ \"./src/components/content/index.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"content\", function() { return _content__WEBPACK_IMPORTED_MODULE_1__[\"default\"]; });\n\n/* harmony import */ var _footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./footer */ \"./src/components/footer/index.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"footer\", function() { return _footer__WEBPACK_IMPORTED_MODULE_2__[\"default\"]; });\n\n/* harmony import */ var _menus__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./menus */ \"./src/components/menus/index.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"menus\", function() { return _menus__WEBPACK_IMPORTED_MODULE_3__[\"default\"]; });\n\n/* harmony import */ var _buttons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./buttons */ \"./src/components/buttons/index.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"buttons\", function() { return _buttons__WEBPACK_IMPORTED_MODULE_4__[\"default\"]; });\n\n/* harmony import */ var _scrolls__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./scrolls */ \"./src/components/scrolls/index.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"scrolls\", function() { return _scrolls__WEBPACK_IMPORTED_MODULE_5__[\"default\"]; });\n\n\n\n\n\n\n\n\n\n//# sourceURL=webpack:///./src/components/index.js?");

/***/ }),

/***/ "./src/components/menus/burger.js":
/*!****************************************!*\
  !*** ./src/components/menus/burger.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar styles = {\n  line: \"\\n\\t\\tdisplay: block;\\n\\t\\twidth: 33px;\\n\\t\\theight: 4px;\\n\\t\\tmargin-bottom: 5px;\\n\\t\\tposition: relative;\\n\\t\\tbackground: #000;\\n\\t\\tborder-radius: 3px;\\n\\t\"\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (\"\\n\\t<span style='\".concat(styles.line, \"'></span>\\n\\t<span style='\").concat(styles.line, \"'></span>\\n\\t<span style='\").concat(styles.line, \"'></span>\\n \"));\n\n//# sourceURL=webpack:///./src/components/menus/burger.js?");

/***/ }),

/***/ "./src/components/menus/index.js":
/*!***************************************!*\
  !*** ./src/components/menus/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _burger__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./burger */ \"./src/components/menus/burger.js\");\n\nvar menus = {\n  burger: _burger__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n};\nvar styles = {\n  half: {\n    backgroundColor: '#000',\n    position: 'absolute',\n    top: '0',\n    left: '0',\n    minWidth: '50vw',\n    minHeight: '100vh',\n    zIndex: '8',\n    opacity: '.8',\n    display: 'flex',\n    justifyContent: 'center',\n    visibility: 'hidden'\n  }\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (function (type, style) {\n  return {\n    menu: menus[type],\n    parentStyles: styles[style]\n  };\n});\n\n//# sourceURL=webpack:///./src/components/menus/index.js?");

/***/ }),

/***/ "./src/components/scrolls/bold.js":
/*!****************************************!*\
  !*** ./src/components/scrolls/bold.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  scrollbar: {\n    width: '20px'\n  },\n  scrollbarThumb: {\n    background: '#fff',\n    border: '2px solid black'\n  }\n});\n\n//# sourceURL=webpack:///./src/components/scrolls/bold.js?");

/***/ }),

/***/ "./src/components/scrolls/index.js":
/*!*****************************************!*\
  !*** ./src/components/scrolls/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _bold__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bold */ \"./src/components/scrolls/bold.js\");\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  bold: _bold__WEBPACK_IMPORTED_MODULE_0__[\"default\"]\n});\n\n//# sourceURL=webpack:///./src/components/scrolls/index.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils */ \"./src/utils/index.js\");\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\n\n\nvar MotifJS = function MotifJS() {\n  var _this = this;\n\n  _classCallCheck(this, MotifJS);\n\n  this.render = function (details) {\n    var templater = new _utils__WEBPACK_IMPORTED_MODULE_0__[\"Templater\"](details, _this.Stylizer);\n    var pages = templater.build();\n    return pages;\n  };\n\n  this.Stylizer = _utils__WEBPACK_IMPORTED_MODULE_0__[\"Stylizer\"];\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (new MotifJS());\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/templates/blank/index.js":
/*!**************************************!*\
  !*** ./src/templates/blank/index.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Blank; });\n/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../components */ \"./src/components/index.js\");\nfunction _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }\n\nfunction _nonIterableRest() { throw new TypeError(\"Invalid attempt to destructure non-iterable instance\"); }\n\nfunction _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i[\"return\"] != null) _i[\"return\"](); } finally { if (_d) throw _e; } } return _arr; }\n\nfunction _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\n\nvar full = {\n  header: _components__WEBPACK_IMPORTED_MODULE_0__[\"header\"],\n  content: _components__WEBPACK_IMPORTED_MODULE_0__[\"content\"],\n  footer: _components__WEBPACK_IMPORTED_MODULE_0__[\"footer\"]\n};\n\nvar Blank = function Blank(details) {\n  _classCallCheck(this, Blank);\n\n  this.parts = Object.entries(details).reduce(function (acc, part) {\n    var _part = _slicedToArray(part, 2),\n        className = _part[0],\n        meta = _part[1];\n\n    if (!meta.type) throw new Error(\"\".concat(meta.type, \" is not a valid \").concat(className, \" type\"));\n    var component = full[className][meta.type];\n    acc.push(component);\n    return acc;\n  }, []);\n};\n\n\n\n//# sourceURL=webpack:///./src/templates/blank/index.js?");

/***/ }),

/***/ "./src/templates/index.js":
/*!********************************!*\
  !*** ./src/templates/index.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _simple__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./simple */ \"./src/templates/simple/index.js\");\n/* harmony import */ var _blank__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./blank */ \"./src/templates/blank/index.js\");\n\n\nvar templates = {\n  simple: _simple__WEBPACK_IMPORTED_MODULE_0__[\"default\"],\n  blank: _blank__WEBPACK_IMPORTED_MODULE_1__[\"default\"]\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (function (type) {\n  return function (parts) {\n    return new templates[type](parts);\n  };\n});\n\n//# sourceURL=webpack:///./src/templates/index.js?");

/***/ }),

/***/ "./src/templates/simple/index.js":
/*!***************************************!*\
  !*** ./src/templates/simple/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Simple; });\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar full = {\n  header: Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ../../components/header/simple */ \"./src/components/header/simple.js\")),\n  content: Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ../../components/content/simple */ \"./src/components/content/simple.js\")),\n  footer: Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ../../components/footer/simple */ \"./src/components/footer/simple.js\"))\n};\n\nvar Simple = function Simple(mainParts) {\n  _classCallCheck(this, Simple);\n\n  this.parts = Object.keys(mainParts).reduce(function (acc, part) {\n    if (full[part]) {\n      var connect = full[part];\n      acc.push(connect);\n    }\n\n    return acc;\n  }, []);\n};\n\n\n\n//# sourceURL=webpack:///./src/templates/simple/index.js?");

/***/ }),

/***/ "./src/utils/Builder.js":
/*!******************************!*\
  !*** ./src/utils/Builder.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Builder; });\nfunction _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }\n\nfunction _nonIterableRest() { throw new TypeError(\"Invalid attempt to destructure non-iterable instance\"); }\n\nfunction _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i[\"return\"] != null) _i[\"return\"](); } finally { if (_d) throw _e; } } return _arr; }\n\nfunction _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }\n\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Builder = function Builder() {\n  _classCallCheck(this, Builder);\n\n  this.sizeToSet = function (size) {\n    return \"\\n\\t\\t\\theight: \".concat(!isNaN(Number(size)) ? size : 100, \"%;\\n\\t\\t\\tmin-width: 100%;\\n\\t\\t\");\n  };\n\n  this.createElement = function (parent, type) {\n    var addtionalOptions = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};\n    var div = document.createElement(type);\n    Object.assign(div, _objectSpread({}, addtionalOptions));\n    parent.appendChild(div);\n    return div;\n  };\n\n  this.returnHTMLString = function (parts) {\n    return parts.map(function (part) {\n      return part.outerHTML;\n    });\n  };\n\n  this.createLinkOnElement = function (targetDiv, link) {\n    targetDiv.onclick = function () {\n      return window.open(link);\n    };\n  };\n\n  this.createVisibilityToggle = function (targetDiv, divToToggle) {\n    targetDiv.onclick = function () {\n      var _document$getElements = document.getElementsByClassName('nav'),\n          _document$getElements2 = _slicedToArray(_document$getElements, 1),\n          targetClass = _document$getElements2[0];\n\n      var currentVisibility = targetClass.style.visibility;\n      targetClass.style.visibility = currentVisibility === 'hidden' ? 'visible' : 'hidden';\n\n      var _document$getElements3 = document.getElementsByClassName('content'),\n          _document$getElements4 = _slicedToArray(_document$getElements3, 1),\n          contentClass = _document$getElements4[0];\n\n      contentClass.onclick = function () {\n        return targetClass.style.visibility = 'hidden';\n      };\n    };\n  };\n};\n\n\n\n//# sourceURL=webpack:///./src/utils/Builder.js?");

/***/ }),

/***/ "./src/utils/Stylizer/Stylizer.js":
/*!****************************************!*\
  !*** ./src/utils/Stylizer/Stylizer.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Stylizer; });\nfunction _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }\n\nfunction _nonIterableRest() { throw new TypeError(\"Invalid attempt to destructure non-iterable instance\"); }\n\nfunction _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i[\"return\"] != null) _i[\"return\"](); } finally { if (_d) throw _e; } } return _arr; }\n\nfunction _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Stylizer = function Stylizer() {\n  var _this = this;\n\n  _classCallCheck(this, Stylizer);\n\n  this.stylizeParent = function (parts) {\n    var parentStyles = _this.extractParentsStyles(parts);\n\n    _this.setParentSizes(parentStyles, parts[1]);\n\n    _this.setParentPositions(parentStyles, parts[1]);\n\n    _this.applyReset();\n\n    return parts;\n  };\n\n  this.applyReset = function () {\n    document.body.style.cssText = \"\\n\\t\\t\\tmargin: 0; \\n\\t   \\t\\tpadding: 0;\\n\\t   \\t\\tborder: 0;\\n\\t   \\t\\tfont-size: 100%;\\n\\t   \\t\\tfont: inherit;\\n\\t   \\t\\tvertical-align: baseline;\\n\\t   \\t\\tcursor: pointer;\\n\\t\\t\";\n  };\n\n  this.extractParentsStyles = function (parts) {\n    return parts.reduce(function (acc, curr) {\n      var className = curr.className,\n          style = curr.style;\n      acc[className] = {\n        width: style.width,\n        height: style.height,\n        top: style.top,\n        bottom: style.bottom,\n        right: style.right,\n        left: style.left\n      };\n      return acc;\n    }, {});\n  };\n\n  this.setTargetStyle = function (target, string) {\n    target.style.cssText += string;\n  };\n\n  this.setParentSizes = function (parentStyles, middle) {\n    var header = parentStyles.header,\n        footer = parentStyles.footer;\n\n    var _filter$reduce = [header, footer].filter(function (exist) {\n      return exist;\n    }).reduce(function (acc, curr) {\n      Object.entries(curr).forEach(function (c) {\n        var _c = _slicedToArray(c, 2),\n            type = _c[0],\n            num = _c[1];\n\n        if (num && num.slice(0, 3) !== '100') {\n          if (!acc[type]) acc[type] = num;else acc[type] += \" + \".concat(num);\n        }\n      });\n      return acc;\n    }, {}),\n        height = _filter$reduce.height,\n        width = _filter$reduce.width;\n\n    var widthToCalc = \"calc(\".concat(middle.style.width, \" - \").concat(width, \")\");\n\n    _this.setTargetStyle(middle, \"width: \".concat(widthToCalc));\n\n    var heightToCalc = \"calc(\".concat(middle.style.height, \" - (\").concat(height, \"))\");\n\n    _this.setTargetStyle(middle, \"height: \".concat(heightToCalc));\n\n    return middle;\n  };\n\n  this.setParentPositions = function (parentStyles, middle) {\n    var header = parentStyles.header,\n        footer = parentStyles.footer;\n\n    if (header) {\n      var top = header.top,\n          height = header.height,\n          left = header.left;\n      if (!top) _this.setTargetStyle(middle, 'top: 0; position: fixed');else _this.setTargetStyle(middle, \"top: \".concat(height, \"; position: fixed\"));\n      if (left) _this.setTargetStyle(middle, \"left: \".concat(header.width, \"; position: fixed\"));\n    }\n\n    return middle;\n  };\n\n  this.setScroll = function (target, scroll) {\n    var direction = scroll.direction,\n        type = scroll.type;\n    target.style.cssText += _this.scrolls.directions[direction || 'vertical'];\n\n    if (type) {\n      var styles = _this.scrolls.types[type]; // this.stylizePsuedoElements(target, styles);\n    }\n  };\n\n  this.setButton = function (target, buttonType) {\n    var buttonStyles = _this.buttons(buttonType);\n\n    return target.style.cssText += buttonStyles;\n  };\n\n  this.setMenu = function (parent, component) {\n    var type = component.type,\n        style = component.style;\n\n    var _this$menus = _this.menus(type, style),\n        menu = _this$menus.menu,\n        parentStyles = _this$menus.parentStyles;\n\n    parent.innerHTML = menu;\n    return {\n      parentStyles: parentStyles\n    };\n  };\n\n  this.stylizePsuedoElements = function (target, psuedoStyles) {\n    return Object.entries(psuedoStyles).forEach(function (style) {\n      var _style = _slicedToArray(style, 2),\n          p = _style[0],\n          s = _style[1];\n\n      var el = _this.transformer.pseudoElements[p];\n\n      var stringStyle = _this.transformStyles(s);\n\n      target.style.insertRule(\".\".concat(target.className).concat(el, \"{\").concat(stringStyle, \"}\"), 0);\n    });\n  };\n\n  this.transformStyles = function (styles) {\n    return Object.entries(styles).reduce(function (acc, curr) {\n      if (!curr) return acc;\n\n      var _curr = _slicedToArray(curr, 2),\n          attr = _curr[0],\n          val = _curr[1];\n\n      if (_this.transformer[attr]) attr = _this.transformer[attr];\n      acc += attr + ':' + val + ';';\n      return acc;\n    }, '');\n  };\n\n  this.stylize = function (target, styles) {\n    return target.style.cssText += _this.transformStyles(styles);\n  };\n\n  for (var _len = arguments.length, dependencies = new Array(_len), _key = 0; _key < _len; _key++) {\n    dependencies[_key] = arguments[_key];\n  }\n\n  Object.assign.apply(Object, [this].concat(dependencies));\n};\n\n\n\n//# sourceURL=webpack:///./src/utils/Stylizer/Stylizer.js?");

/***/ }),

/***/ "./src/utils/Stylizer/index.js":
/*!*************************************!*\
  !*** ./src/utils/Stylizer/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Stylizer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Stylizer */ \"./src/utils/Stylizer/Stylizer.js\");\n/* harmony import */ var _styleConstants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styleConstants */ \"./src/utils/Stylizer/styleConstants.js\");\n/* harmony import */ var _styleTransformer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styleTransformer */ \"./src/utils/Stylizer/styleTransformer.js\");\n/* harmony import */ var _styleScrolls__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./styleScrolls */ \"./src/utils/Stylizer/styleScrolls.js\");\n/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components */ \"./src/components/index.js\");\n\n\n\n\n\nvar dependencies = {\n  constants: _styleConstants__WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  transformer: _styleTransformer__WEBPACK_IMPORTED_MODULE_2__[\"default\"],\n  scrolls: _styleScrolls__WEBPACK_IMPORTED_MODULE_3__[\"default\"],\n  buttons: _components__WEBPACK_IMPORTED_MODULE_4__[\"buttons\"],\n  menus: _components__WEBPACK_IMPORTED_MODULE_4__[\"menus\"]\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (new _Stylizer__WEBPACK_IMPORTED_MODULE_0__[\"default\"](dependencies));\n\n//# sourceURL=webpack:///./src/utils/Stylizer/index.js?");

/***/ }),

/***/ "./src/utils/Stylizer/styleConstants.js":
/*!**********************************************!*\
  !*** ./src/utils/Stylizer/styleConstants.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar constants = {\n  screen: {\n    minHeight: '100vh',\n    minWidth: '100vw'\n  },\n  fullContent: {\n    minHeight: '100%',\n    minWidth: '100vw'\n  },\n  halfSpread: {\n    display: 'flex',\n    justifyContent: 'space-between',\n    width: '50vw'\n  },\n  center: {\n    display: 'flex',\n    justifyContent: 'center',\n    alignItems: 'center'\n  },\n  row: {\n    display: 'flex',\n    flexDirection: 'row'\n  },\n  column: {\n    display: 'flex',\n    flexDirection: 'column'\n  }\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (constants);\n\n//# sourceURL=webpack:///./src/utils/Stylizer/styleConstants.js?");

/***/ }),

/***/ "./src/utils/Stylizer/styleScrolls.js":
/*!********************************************!*\
  !*** ./src/utils/Stylizer/styleScrolls.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../components */ \"./src/components/index.js\");\n\nvar directions = {\n  vertical: \"\\n\\t\\toverflow-y: auto;\\n  \\t\",\n  horizontal: \"\\n\\t\\tdisplay: flex;\\n  \\t\\toverflow-x: auto;\\n  \\t\\toverflow-y: hidden;\\n  \\t\"\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  types: _components__WEBPACK_IMPORTED_MODULE_0__[\"scrolls\"],\n  directions: directions\n});\n\n//# sourceURL=webpack:///./src/utils/Stylizer/styleScrolls.js?");

/***/ }),

/***/ "./src/utils/Stylizer/styleTransformer.js":
/*!************************************************!*\
  !*** ./src/utils/Stylizer/styleTransformer.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar pseudoElements = {\n  scrollbar: '::-webkit-scrollbar',\n  scrollbarTrack: '::-webkit-scrollbar-track',\n  scrollbarThumb: '::-webkit-scrollbar-thumb',\n  scrollarThumbHover: '::-webkit-scrollbar-thumb:hover'\n};\nvar transformer = {\n  backgroundColor: 'background-color',\n  textColor: 'color',\n  fontSize: 'font-size',\n  justifyContent: 'justify-content',\n  alignItems: 'align-items',\n  borderBottom: 'border-bottom',\n  paddingLeft: 'padding-left',\n  paddingRight: 'padding-right',\n  marginLeft: 'margin-left',\n  marginRight: 'margin-right',\n  maxWidth: 'max-width',\n  minWidth: 'min-width',\n  maxHeight: 'max-height',\n  minHeight: 'min-height',\n  flexDirection: 'flex-direction',\n  zIndex: 'z-index',\n  pseudoElements: pseudoElements\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (transformer);\n\n//# sourceURL=webpack:///./src/utils/Stylizer/styleTransformer.js?");

/***/ }),

/***/ "./src/utils/Templater.js":
/*!********************************!*\
  !*** ./src/utils/Templater.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Templater; });\n/* harmony import */ var _Builder__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Builder */ \"./src/utils/Builder.js\");\n/* harmony import */ var _templates__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../templates */ \"./src/templates/index.js\");\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }\n\nfunction _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }\n\nfunction _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }\n\nfunction _nonIterableRest() { throw new TypeError(\"Invalid attempt to destructure non-iterable instance\"); }\n\nfunction _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i[\"return\"] != null) _i[\"return\"](); } finally { if (_d) throw _e; } } return _arr; }\n\nfunction _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\n\n\n\nvar Templater =\n/*#__PURE__*/\nfunction (_Builder) {\n  _inherits(Templater, _Builder);\n\n  function Templater(_details, stylizer) {\n    var _this;\n\n    _classCallCheck(this, Templater);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(Templater).call(this));\n\n    _this.init = function (template, main) {\n      _this.activeTemplate = !template ? Object(_templates__WEBPACK_IMPORTED_MODULE_1__[\"default\"])('blank')(main) : Object(_templates__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(template)(main);\n      return _this.activeTemplate;\n    };\n\n    _this.renderImages = function (renderTo, image, size) {\n      var div = _this.createElement(renderTo, 'div', {});\n\n      if (size) {\n        var sizeToSet = _this.sizeToSet(size);\n\n        div.style.cssText += sizeToSet;\n      }\n\n      var img = new Image();\n      img.src = image;\n      img.style.cssText += 'max-height: 100%; min-width: 100%;';\n      div.appendChild(img);\n      return div;\n    };\n\n    _this.renderTextContainer = function (renderTo, singleOrArrayOfText) {\n      var target = !Array.isArray(singleOrArrayOfText) ? [singleOrArrayOfText] : singleOrArrayOfText;\n      return target.map(function (txt) {\n        var text = txt.text;\n\n        var div = _this.createElement(renderTo, 'div', {\n          innerHTML: text || txt\n        });\n\n        return div;\n      });\n    };\n\n    _this.renderSections = function (renderTo, sections) {\n      var scroll = sections.scroll,\n          views = sections.views;\n      if (scroll) _this.Stylizer.setScroll(renderTo, scroll);\n      if (!views) throw new Error('Must provide views in sections.');\n\n      _this.renderViews(renderTo, views);\n    };\n\n    _this.renderButtomContainer = function (renderTo, buttonContainer) {\n      var type = buttonContainer.type,\n          text = buttonContainer.button;\n\n      var div = _this.createElement(renderTo, 'button', {\n        innerHTML: text\n      });\n\n      _this.Stylizer.setButton(div, type);\n\n      return div;\n    };\n\n    _this.renderView = function (container, view) {\n      view.forEach(function (c) {\n        var result = null;\n        var text = c.text,\n            innerStyles = c.styles,\n            image = c.image,\n            link = c.link,\n            button = c.button;\n        if (image) result = _this.renderImages(container, image);\n        if (text) result = _this.renderTextContainer(container, text);\n        if (button) result = _this.renderButtomContainer(container, c);\n        if (link) _this.createLinkOnElement(result, link);\n\n        if (innerStyles) {\n          var target = !Array.isArray(result) ? [result] : result;\n          target.forEach(function (t) {\n            return _this.Stylizer.stylize(t, innerStyles);\n          });\n        }\n      });\n    };\n\n    _this.renderViews = function (renderTo, views) {\n      if (!views.length) return;\n\n      var _views$splice = views.splice(0, 1),\n          _views$splice2 = _slicedToArray(_views$splice, 1),\n          first = _views$splice2[0];\n\n      var scroll = first.scroll,\n          styles = first.styles,\n          component = first.component,\n          view = _objectWithoutProperties(first, [\"scroll\", \"styles\", \"component\"]);\n\n      var container = _this.createElement(renderTo, 'div', {\n        className: className\n      });\n\n      if (component) {\n        var details = component.details;\n\n        var _this$Stylizer$setMen = _this.Stylizer.setMenu(container, component),\n            parentStyles = _this$Stylizer$setMen.parentStyles;\n\n        var nav = _this.createElement(document.body, 'div', {\n          className: 'nav'\n        });\n\n        _this.Stylizer.stylize(nav, parentStyles);\n\n        _this.createVisibilityToggle(container, nav);\n\n        return _this.renderView(nav, details);\n      }\n\n      var _Object$entries = Object.entries(view),\n          _Object$entries2 = _slicedToArray(_Object$entries, 1),\n          _Object$entries2$ = _slicedToArray(_Object$entries2[0], 2),\n          className = _Object$entries2$[0],\n          targetView = _Object$entries2$[1];\n\n      if (scroll) _this.Stylizer.setScroll(container, scroll);\n      if (styles) _this.Stylizer.stylize(container, styles);\n\n      _this.renderView(container, targetView);\n\n      return _this.renderViews(renderTo, views);\n    };\n\n    _this.createChildrenDivs = function (targetDOM, toAdd) {\n      Object.entries(toAdd).forEach(function (add) {\n        var _add = _slicedToArray(add, 2),\n            className = _add[0],\n            targetToAdd = _add[1];\n\n        for (var i = 0; i < targetDOM.length; i++) {\n          var curr = targetDOM[i];\n\n          if (curr.className === className) {\n            var styles = targetToAdd.styles,\n                sections = targetToAdd.sections;\n            if (styles) _this.Stylizer.stylize(curr, styles);\n\n            _this.renderSections(curr, sections);\n\n            break;\n          }\n        }\n      });\n    };\n\n    _this.createParentDivs = function (parts) {\n      parts.forEach(function (part, index) {\n        var type = part.type,\n            styles = part.styles,\n            rest = _objectWithoutProperties(part, [\"type\", \"styles\"]);\n\n        var div = _this.createElement(document.body, type, rest);\n\n        if (styles) _this.Stylizer.stylize(div, styles);\n        _this.activeTemplate.parts[index] = div;\n      });\n      return parts;\n    };\n\n    _this.build = function () {\n      var _this$details = _this.details,\n          template = _this$details.template,\n          main = _this$details.main;\n\n      _this.init(template, main);\n\n      var parts = _this.activeTemplate.parts;\n\n      _this.createParentDivs(parts);\n\n      _this.createChildrenDivs(parts, main);\n\n      var styled = _this.Stylizer.stylizeParent(parts);\n\n      var results = _this.returnHTMLString(styled);\n\n      return results.join('');\n    };\n\n    if (!_details) return _possibleConstructorReturn(_this, 'Must provide details.');\n    _this.Stylizer = stylizer;\n    _this.details = _details;\n    _this.activeTemplate = null;\n    return _this;\n  }\n  /**\n   * @param {string} template\n   * @param {object} main\n   */\n\n\n  return Templater;\n}(_Builder__WEBPACK_IMPORTED_MODULE_0__[\"default\"]);\n\n\n\n//# sourceURL=webpack:///./src/utils/Templater.js?");

/***/ }),

/***/ "./src/utils/index.js":
/*!****************************!*\
  !*** ./src/utils/index.js ***!
  \****************************/
/*! exports provided: Templater, Stylizer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Templater__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Templater */ \"./src/utils/Templater.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Templater\", function() { return _Templater__WEBPACK_IMPORTED_MODULE_0__[\"default\"]; });\n\n/* harmony import */ var _Stylizer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Stylizer */ \"./src/utils/Stylizer/index.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Stylizer\", function() { return _Stylizer__WEBPACK_IMPORTED_MODULE_1__[\"default\"]; });\n\n\n\n\n\n//# sourceURL=webpack:///./src/utils/index.js?");

/***/ })

/******/ });